//
//  CarProtocol.swift
//  GSDKDemo
//
//  Created by KingYoung on 2020/05/8.
//  Copyright © 2020 xuxun. All rights reserved.
//

import Foundation



public protocol CarControl {
    
    func remoteLock(completion: @escaping (Result<String, Error>) -> Void)
    
    func remoteUnlock()
}

extension CarControl {
    // 默认空实现，相当于该协议方法是可选的
    func remoteUnlock() {
        
    }
    
}


public protocol CarStatus {
    
    
    
}


public protocol CarConfig {
    
    associatedtype mbbVehicle
    
    
    
    
    func mbbSetup()
    func mbbLogin()
}

extension CarConfig {
    func mbbSetup() {
        
        
    }
    
    func mbbLogin() {
        
        
    }
    
}
