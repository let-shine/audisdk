//
//  MBBManager.swift
//  GSDKDemo
//
//  Created by KingYoung on 2020/05/8.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import UIKit
//import MBBConnector


class MBBManager {
    
    let falBaseURL = URL(string: "https://msg.audi.de/fs-car/")!
    let malBaseURL = URL(string: "https://mal-1a.prd.ece.vwg-connect.com/api/")!
    let oAuthURL = URL(string: "https://mbboauth-1d.prd.ece.vwg-connect.com/mbbcoauth")!

//
//    var mbbConnector: Connector?
//    var vehicle: Vehicle?
//
//    func setup() {
//        let client = Client(requestManager: URLSessionRequestManager())
//        let app = App(brand: .audi,
//                      name: "MBBDemo",
//                      version: "1.0",
//                      id: Bundle.main.bundleIdentifier!)
//
//        mbbConnector = Connector(client: client,
//                                 app: app,
//                                 operationListVersion: .v3,
//                                 falBaseURL: falBaseURL,
//                                 malBaseURL: malBaseURL,
//                                 oAuthURL: oAuthURL,
//                                 keychainTokenPrefix: "MBBConnector",
//                                 stageIdentifier: "Live",
//                                 targetSecurePINVersion: .v2,
//                                 useInvocationURLs: false,
//                                 expiredTokenHandler: nil)
//    }
//
//    func login() {
//        mbbConnector.authenticate(with: "token"!, clientName: UIDevice.current.name) { error in
//
//            if let error = error {
//
//            } else {
//                vehicle = Vehicle(vin: "vin", connector: mbbConnector)
//
//            }
//        }
//    }
}


typealias RemoteService = MBBManager
extension RemoteService {
    func remoteLock(completion: @escaping (Result<String, Error>) -> Void) {
        
//        vehicle.remoteLockUnlock.lock(with: "securePIN", completion: nil)
    }
    
    func remoteUnlock(completion: @escaping (Result<String, Error>) -> Void) {
        
//        vehicle.remoteLockUnlock.unlock(with: "securePIN", completion: nil)
    }
    
    
}
