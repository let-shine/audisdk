//
//  LocalizeKeyConfig.swift
//  MosProject
//
//  Created by 余鹏 on 2019/7/20.
//  Copyright © 2019 SVW. All rights reserved.
//

import Foundation

/* 增加一个新的module 需要配置 */
/* 例：新增品牌模块brand <brand 为本地化文件名>
 typealias Common = LocalizeKey.common
 extension Common: LocalizeKeyStringProtocol {}
 
 typealias Brand = LocalizeKey.brand
 extension Brand: LocalizeKeyStringProtocol {}
 /// 使用
 /// LK(Brand.cancel)
 /// LK(Brand.confirm)
 */

//错误
private typealias Error = LocalizeKey.error
extension Error: LocalizeKeyStringProtocol {}

/// 通用
private typealias Common = LocalizeKey.common
extension Common: LocalizeKeyStringProtocol {}

protocol LocalizeKeyStringProtocol {
    var rawValue: String { get }
}



/*********************** 分割线 *************************/

/// 获取国际化字符串
///
/// - Parameters:
///   - key: LocalizeKey枚举值<字符串对应的枚举>
///   - name: 本地化文件别名
/// - Returns: 国际化后字符串
func LK(_ key: LocalizeKeyStringProtocol) -> String {
    
    guard let bundleURL = Bundle.main.url(forResource: "Resource", withExtension: "bundle") else { fatalError("AudiLocalizeKey bundleURL can't be nil") }
    
    guard let resourceBundle = Bundle(url: bundleURL) else { fatalError("AudiLocalizeKey resourceBundle can't be nil")}
    
    let tableName = "\(type(of: key))"
    
    let str = NSLocalizedString(key.rawValue, tableName: tableName, bundle: resourceBundle, value: "", comment: "")
    return str
}
