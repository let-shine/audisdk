
import UIKit


/**#LocalizeKey*/
enum LocalizeKey: String {

    /*#keys-Localize*/
    /// Invalid key. Never use
    case invalidKey = "invalidKey"
    /// 积分
    case point = "point"

/*#end-keys-Localize*/
    
    
    /**#common*/
    enum common: String {

        /*#keys-common*/
        case tips = "common_tips"
	/*#end-keys-common*/

        /*#end-common*/
    }
    
    enum error: String {
        case expiredToken = "error_expiredToken"
    }

/*#end-Localize*/
}


