//
//  AD_ErrorHelper.swift
//  GSDKDemo
//
//  Created by KingYoung on 2020/05/19.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import Foundation
import MBBConnector

protocol AD_ErrorHandler {
    func convertGreyErrorToAppError(error: MBBError)->AD_AppError?
}

struct AD_AuthErrorConvertStrategy: AD_ErrorHandler {
    func convertGreyErrorToAppError(error: MBBError) -> AD_AppError? {
       
        var targetError: MBBConnector.MBBAuthError?
        switch error {
        case .auth(let authError):
            targetError = authError
        default:
            break
        }
        
        var code = ""
        var msg = ""
        var type: AD_AppError.AuthError =  .unknown
        switch targetError {

//        /// An error while storing or retrieving a token from the keychain occurred. The underlying error is passed as the associated value.
//        case .keychainError(let error)
//
//        /// No _Refresh Token_ could be found in the keychain
//        case .noTokenAvailable
//
//        /// A token does not contain all required information
//        case .invalidTokenFormat

        /// The _Refresh Token_ expired
        case .expiredToken:
            type = .expiredToken
            code = "expiredToken"
            msg = LK(LocalizeKey.error.expiredToken)

        /// An authorized request failed with an underlying error
//        case .authorizationError(MBBConnector.MBBError)
        @unknown default:
            break
        }
        return .auth(type: type, code: code, msg: msg)
        
    }
}






enum AD_AppError {
    enum AuthError {
  
        case expiredToken
        /**
         * 新版更新，未及时转换
         */
        case unknown
    }
    
    case auth(type: AuthError, code: String, msg: String)
    
    public static func convertGreyErroToAppError(error: MBBError)->AD_AppError? {
        var convertStrategy: AD_ErrorHandler?
        switch error {
        case .auth:
            convertStrategy = AD_AuthErrorConvertStrategy()
        default:
            break
        }
        return convertStrategy?.convertGreyErrorToAppError(error: error)
    }
}
