## MBBConnector
### Login
* token设置和失效问题

### Vehicle
* reloadOperationList 获取操作列表
* vehicleStatusReport.refresh 更新状态

### MBB Service
* All objects representing MBB Services can be serialized and deserialized
* report
* action - the model representation of a PendingAction.
* pending action - 远程操作的描述对象
* PendingActionCoordinator - PendingActions的管理类，更新PendingActions状态，其状态可被监听，清理(手动)失败的pending actions

### Push Notifications
* 推送包含字段
    * serviceId - the Operation List Service the push notification belongs to
    * vin - the Vehicle which triggered the push notification 
    * rawTaskValue - the task the push notification has been triggered for
    
* 客户端可以push notification给MBB

* MBB可以处理一些收到的推送

## HTTPClient
* 定义request
    1. define three associated types:
        ResultPayloadType
        ErrorPayloadType
        ConnectorErrorType
    2. 重写request()相关方法
    3. 重写resultPayload()方法
    
* 定义响应json数据解析后的对象，即具体的ResultPayloadType
    继承JSONDecodable
    
* If you are not expecting a response to your request, use 
'typealias ResultPayloadType = EmptyPayload'

* Authentication
    1. Authorized requires one additional property of type AuthorizationProvider


## 一些问题
* URLSessionRequestManager(policies: ["my.host.com" : .disableEvaluation]) 参数怎么传
* post请求
* MBBConnector 中Login操作中token失效了如何处理
* reloadOperationList和vehicleStatusReport.refresh 的执行顺序
