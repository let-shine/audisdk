//
//  GSDKDemoTests.swift
//  GSDKDemoTests
//
//  Created by KingYoung on 2020/05/7.
//  Copyright © 2020 KingYoung. All rights reserved.
//

@testable import GSDKDemo
import XCTest
import Logger
import MBBConnector
import HTTPClient


let identityToken = "eyJraWQiOiIwMDAwMyIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJXODUwMjUwNDk4Iiwic2NwIjpbInByb2ZpbGUiLCJtYmIiXSwidmVyIjoiMSIsImFtciI6InB3ZCIsImNvciI6IkNOIiwia2lkIjoiMDAwMDMiLCJpc3MiOiJodHRwczpcL1wvcHJlLWlkLmF1ZGkuY29tXC92MVwvIiwidHlwZSI6ImlkZW50aXR5IiwibG9jYWxlIjoiemgtQ04iLCJhdWQiOlsiVldHTUJCMDFERVBSRTIiLCJWV0dNQkIwMURFUFJFMSIsIlZXR01CQjAxREVBUFAxIiwiVldHTUJCMDFDTkFQUDEiLCJWV0dNQkIwMUNOUFJFMSIsIlZXR01CQjAxQ05QUkwxIiwibW1pY29ubmVjdF9pb3MiXSwiYXpwIjoibW1pY29ubmVjdF9pb3MiLCJleHAiOjE1ODk1MDg2MTgsImlhdCI6MTU4OTUwODAxOCwianRpIjoiYTdiM2I4MDUtY2QzYy00MDI1LTlkODAtYWM0NjAyNDM0Mzk5In0.AlYydwlKCUMG15r0InZazuBc96MMugKzMWd9LjR6wG8aHTAZBDmDvrTp_9fSSM-r9eZmWr1WAyWCn8lrXFeQcsQ0rVejXnJ_sBAAJX6exlvrPckHtPcLjKj6QjQHv0UonX0xWuATo_v5ouctpGSRcvyWsApZTi_FBZLt9uO2epnZnoHUZUF9rrXXYlpO9U1rEBIT98axmrWYKhmI1utgiHILa4-imfUXFi2gEukHhNM9FqYuGaxYjCi5mejgEcl_-JRphyfjplFLDhNn2WrD8WENHhyQpANYtuhqPPdcVmUGpIGBCLtlinJAuxr0ML41mzpP7YmguLj6N8Knzor8fw"
let vin = "BAUNEE4K020050601"

let oAuthURL = URL(string: "https://mbboauth-1d.app.cn.vwg-connect.cn/mbbcoauth")!
let falBaseURL = URL(string: "https://pre-msg.audi-connect.cn/fs-car-ng/")!
let malBaseURL = URL(string: "https://mal-1a.app.cn.vwg-connect.cn/api/")!

let mbb: Connector = {
    let client = Client(requestManager: URLSessionRequestManager())
    let app = App(brand: .audi, name: "MBBDemo", version: "1.0", id: Bundle.main.bundleIdentifier!, pushAppId: nil, platform: .apple)
    return Connector(client: client,
                     app: app,
                     operationListVersion: .v3,
                     falBaseURL: falBaseURL,
                     malBaseURL: malBaseURL,
                     oAuthURL: oAuthURL,
                     keychainTokenPrefix: "MBBConnector",
                     stageIdentifier: "Test",
                     targetSecurePINVersion: .v2,
                     useInvocationURLs: false,
                     persistencyDelegate: nil,
                     persistedData: nil) {
                        
    }
}()

var vehicle:Vehicle?

var bodyStatus:MBBConnector.VehicleStatusReport.StatusReport.BodyStatus?

let latitude = 31.40527
let longitude = 121.48941

let errTag = "\n****************************************"


class GSDKDemoTests: XCTestCase {
    
    override func setUp() {
        testLogin()
        testReloadOperationList()
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLogin() {
       let expect = expectation(description: "timeout")
              print("\(errTag)")
              mbb.authenticate(with: identityToken, clientName: UIDevice.current.name) { error in
                  XCTAssertNil(error, "\(errTag)")
                  vehicle = Vehicle(vin: vin, connector: mbb)
                  expect.fulfill()
              }
              wait(for: [expect], timeout: 10)
    }
    
    func testReloadOperationList() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        vehicle?.reloadOperationList(completion: { result in
            switch result {
            case .changed, .unchanged:
                vehicle?.vehicleStatusReport.refresh { _ in
                    bodyStatus = vehicle?.vehicleStatusReport.report.result?.value?.body
                    expect.fulfill()
                }
            case .error(let error):
                XCTAssertNil(error, "\(errTag)")
                expect.fulfill()
            @unknown default:
                XCTAssert(false, "Unkonwn operation list result encountered")
                expect.fulfill()
            }
            
        })
        wait(for: [expect], timeout: 20)
        
    }
    
    func testReportBodyStatus() {
        XCTAssertNotNil(bodyStatus, "\(errTag)")
        print("\(errTag)")
        print("\(bodyStatus!.sunroof)")
        print("\(errTag)")
    }
    
    func testLock() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.remoteLockUnlock.isLockAllowed {
            vehicle.remoteLockUnlock.lock(with: nil) { error in
                XCTAssertNil(error, "\(errTag)")
                expect.fulfill()
            }
        }
        wait(for: [expect], timeout: 10)
        
    }
    
    func testUnlock() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.remoteLockUnlock.isLockAllowed {
            vehicle.remoteLockUnlock.unlock(with: nil) { error in
                XCTAssertNil(error, "\(errTag)")
                expect.fulfill()
            }
        }
        wait(for: [expect], timeout: 10)
        
    }
    
    func testCarFinderLocation() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.carFinder.isRequestCurrentAllowed {
            vehicle.carFinder.requestCurrentLocation { error in
                XCTAssertNil(error, "\(errTag)")
                expect.fulfill()
            }
        }
        
        wait(for: [expect], timeout: 10)
    }
    
    func testRemoteHonkAndFlash() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.remoteHonkAndFlash.isPerformServiceRequestAllowed {
            vehicle.remoteHonkAndFlash.performServiceRequest(RemoteHonkAndFlash.ServiceRequest(actionType: RemoteHonkAndFlash.Action.ActionType.honkAndFlash, latitude: 31.40527, longitude: 121.48941), with: nil) { (error) in
                XCTAssertNil(error, "\(errTag)")
                expect.fulfill()
            }
            
        }
        
        wait(for: [expect], timeout: 10)
    }
    
    func testRemoteHeatingStart() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.remoteHeating.isQuickStartAllowed {
            vehicle.remoteHeating.quickStart(for: Measurement<UnitDuration>(value: 20, unit: UnitDuration(symbol: "12")), securePIN: nil, completion: { error in
                XCTAssertNil(error, "\(errTag)")
                expect.fulfill()
            })
        }
        
        wait(for: [expect], timeout: 10)
    }
    
    func testRemoteHeatingStop() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.remoteHeating.isQuickStopAllowed {
            vehicle.remoteHeating.quickStop(with: nil, completionQueue: nil) { error in
                XCTAssertNil(error, "\(errTag)")
                expect.fulfill()
            }
        }
        wait(for: [expect], timeout: 10)
    }
    
    func testValetAlertCreateDefinition() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.valetAlert.isCreateDefinitionAllowed {
            vehicle.valetAlert.createDefinition(with: Alert.Circle(coordinate: Coordinate(latitude: 31.40527, longitude: 121.48941)!, radius: 50), and: Measurement<UnitSpeed>.init(value: 50, unit: UnitSpeed.init(symbol: "km"))) { error in
                XCTAssertNil(error, "\(errTag)")
                expect.fulfill()
            }
        }
        
        wait(for: [expect], timeout: 10)
        
    }
    
    func testValetAlertDeleteDefinition() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.valetAlert.isDeleteDefinitionAllowed {
            vehicle.valetAlert.deleteDefinition { error in
                 XCTAssertNil(error, "\(errTag)")
                               expect.fulfill()
            }
        }
        wait(for: [expect], timeout: 10)
    }

    func testSpeedAlert() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.speedAlert.isReloadViolationsAllowed {
            vehicle.speedAlert.reloadViolations { (result) in
                switch result {
                case .failure(let error):
                    XCTAssertNil(error, "\(errTag)")
                    expect.fulfill()
                case .success(let violations):
                    print("\(violations)")
                    expect.fulfill()
                }
            }
            
            
        }
        
        wait(for: [expect], timeout: 10)
        
    }
    
    func testGeofencing() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.geofencing.isSendDefinitionsAllowed {
            vehicle.geofencing.sendDefinitions([Geofencing.Definition.UserDefined(enabled: true, area: Alert.Area(zone: Alert.Area.Zone.green, areaType: Alert.Area.AreaType.ellipse(Alert.Ellipse(coordinate: Coordinate(latitude: latitude, longitude: longitude)!, radius: 10))), schedule: Alert.Schedule.simple(DateInterval(start: Date(timeIntervalSinceNow: 0), duration: 30)))]) { (error) in
                XCTAssertNil(error, "\(errTag)")
                expect.fulfill()
            }
        }
        
        wait(for: [expect], timeout: 10)
        
    }
    
    func testRemoteBatteryChargeStart() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.remoteBatteryCharge.isStartChargingAllowed {
            vehicle.remoteBatteryCharge.startCharging { error in
                XCTAssertNil(error, "\(errTag)")
                expect.fulfill()
            }
        }
        
        wait(for: [expect], timeout: 10)
        
        
    }
    
    func testRemoteBatteryChargeStop() {
           let expect = expectation(description: "timeout")
           print("\(errTag)")
           if let vehicle = vehicle, vehicle.remoteBatteryCharge.isStopChargingAllowed {
               vehicle.remoteBatteryCharge.stopCharging { error in
                   XCTAssertNil(error, "\(errTag)")
                   expect.fulfill()
               }
           }
           
           wait(for: [expect], timeout: 10)
       }
    
    func testRemotePretripClimatisationStart() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.remotePretripClimatisation.isStartWindowHeatingAllowed {
            vehicle.remotePretripClimatisation.startWindowHeating { error in
                XCTAssertNil(error, "\(errTag)")
                expect.fulfill()
            }
        }
        
        wait(for: [expect], timeout: 10)
    }
    
    func testRemotePretripClimatisationStop() {
           let expect = expectation(description: "timeout")
           print("\(errTag)")
           if let vehicle = vehicle, vehicle.remotePretripClimatisation.isStopWindowHeatingAllowed {
               vehicle.remotePretripClimatisation.stopWindowHeating { error in
                   XCTAssertNil(error, "\(errTag)")
                   expect.fulfill()
               }
           }
           
           wait(for: [expect], timeout: 10)
       }

    func testRemoteTripStatistics() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.remoteTripStatistics.isLoadingTripsAllowed {
            vehicle.remoteTripStatistics.reloadTripsReport(for: RemoteTripStatistics.Trip.TripType.longTerm, in: DateInterval(start: Date(timeIntervalSinceNow: 0), duration: 1000), completionQueue: nil) { (result) in
                switch result {
                case .failure(let error):
                    XCTAssertNil(error, "\(errTag)")
                    expect.fulfill()
                case .success(let trips):
                    print("\(trips)")
                    expect.fulfill()
                }
            
            }
        }
        
        wait(for: [expect], timeout: 10)
        
    }
    
    func testDestinationImport() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.destinationImport.isEnabled {
            vehicle.destinationImport.addDestination(with: "四合院", address: nil, coordinate: Coordinate(latitude: latitude, longitude: longitude)!) { (result) in
                switch result {
                case .failure(let error):
                    XCTAssertNil(error, "\(errTag)")
                    expect.fulfill()
                case .success(let dest):
                    print("\(dest)")
                    expect.fulfill()
                }
            }
        }
        
        wait(for: [expect], timeout: 10)
    }
    
    func testTheftAlarm() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.theftAlarm.isLoadingHistoryActivatedAllowed {
            vehicle.theftAlarm.loadHistoryActivated { (result) in
                switch result {
                case .failure(let error):
                    XCTAssertNil(error, "\(errTag)")
                    expect.fulfill()
                case .success(let hist):
                    print("\(hist)")
                    expect.fulfill()
                }
            }
        }
        
        wait(for: [expect], timeout: 10)
        
        
    }
    
    
    func testVehicleTracking() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.vehicleTracking.isLoadingSecureCenterAllowed {
            vehicle.vehicleTracking.loadSecureCenter { (result) in
                switch result {
                case .failure(let error):
                    XCTAssertNil(error, "\(errTag)")
                    expect.fulfill()
                case .success(let hist):
                    print("\(hist)")
                    expect.fulfill()
                }
            }
        }
        
        wait(for: [expect], timeout: 10)
    
    }
  
    
    func testServiceAppointment() {
        let expect = expectation(description: "timeout")
        print("\(errTag)")
        if let vehicle = vehicle, vehicle.serviceAppointment.isLoadingConfigurationAllowed {
            vehicle.serviceAppointment.loadConfiguration { (result) in
                switch result {
                case .failure(let error):
                    XCTAssertNil(error, "\(errTag)")
                    expect.fulfill()
                case .success(let hist):
                    print("\(hist)")
                    expect.fulfill()
                }
            }
        }
        
        wait(for: [expect], timeout: 10)
    }
    
    
    

//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//
//    }
}
