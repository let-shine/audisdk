
@import Foundation;

static NSString *const __nonnull AAFLoggerDefaultLogModule = @"Main";

#define _AAFLog(mLevel, mdl, csi, frmt, ...)                                     \
[AAFLog logWithMessage:[NSString stringWithFormat: frmt, ##__VA_ARGS__]     \
                module:mdl                                                  \
                 level:mLevel                                               \
containsSensitiveInformation:csi                                          \
             timestamp:[NSDate date]                                        \
                  file:@__FILE__                                            \
              function:[NSString stringWithUTF8String:__PRETTY_FUNCTION__]  \
                  line:__LINE__]

#define AAFLogErrorInModule(mdl, frmt, ...) _AAFLog(AAFLogLevelError, mdl, NO, frmt, ##__VA_ARGS__)
#define AAFLogWarningInModule(mdl, frmt, ...) _AAFLog(AAFLogLevelWarning, mdl, NO, frmt, ##__VA_ARGS__)
#define AAFLogInfoInModule(mdl, frmt, ...) _AAFLog(AAFLogLevelInfo, mdl, NO, frmt, ##__VA_ARGS__)
#define AAFLogDebugInModule(mdl, frmt, ...) _AAFLog(AAFLogLevelDebug, mdl, NO, frmt, ##__VA_ARGS__)
#define AAFLogVerboseInModule(mdl, frmt, ...) _AAFLog(AAFLogLevelVerbose, mdl, NO, frmt, ##__VA_ARGS__)

#define AAFLogError(frmt, ...) AAFLogErrorInModule(AAFLoggerDefaultLogModule, frmt, ##__VA_ARGS__)
#define AAFLogWarning(frmt, ...) AAFLogWarningInModule(AAFLoggerDefaultLogModule, frmt, ##__VA_ARGS__)
#define AAFLogInfo(frmt, ...) AAFLogInfoInModule(AAFLoggerDefaultLogModule, frmt, ##__VA_ARGS__)
#define AAFLogDebug(frmt, ...) AAFLogDebugInModule(AAFLoggerDefaultLogModule, frmt, ##__VA_ARGS__)
#define AAFLogVerbose(frmt, ...) AAFLogVerboseInModule(AAFLoggerDefaultLogModule, frmt, ##__VA_ARGS__)

#define AAFLogSensitiveErrorInModule(mdl, frmt, ...) _AAFLog(AAFLogLevelError, mdl, YES, frmt, ##__VA_ARGS__)
#define AAFLogSensitiveWarningInModule(mdl, frmt, ...) _AAFLog(AAFLogLevelWarning, mdl, YES, frmt, ##__VA_ARGS__)
#define AAFLogSensitiveInfoInModule(mdl, frmt, ...) _AAFLog(AAFLogLevelInfo, mdl, YES, frmt, ##__VA_ARGS__)
#define AAFLogSensitiveDebugInModule(mdl, frmt, ...) _AAFLog(AAFLogLevelDebug, mdl, YES, frmt, ##__VA_ARGS__)
#define AAFLogSensitiveVerboseInModule(mdl, frmt, ...) _AAFLog(AAFLogLevelVerbose, YES, mdl, frmt, ##__VA_ARGS__)
