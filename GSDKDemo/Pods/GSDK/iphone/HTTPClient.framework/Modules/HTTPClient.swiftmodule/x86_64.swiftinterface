// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.2.2 (swiftlang-1103.0.32.6 clang-1103.0.32.51)
// swift-module-flags: -target x86_64-apple-ios12.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name HTTPClient
import Foundation
import Logger
import Observing
import Security
import Swift
import Utility
public protocol RequestBuilder {
  var acceptableStatusCodes: [HTTPClient.StatusCode] { get }
  func request(with authorization: HTTPClient.Authorization) throws -> HTTPClient.HTTPRequest
  func request() throws -> HTTPClient.HTTPRequest
  func request(with authorizationHeaders: [HTTPClient.Header : Swift.String]) throws -> HTTPClient.HTTPRequest
  func request(with authorizationParams: [HTTPClient.Parameter]) throws -> HTTPClient.HTTPRequest
}
extension RequestBuilder {
  public var acceptableStatusCodes: [HTTPClient.StatusCode] {
    get
  }
  public func request(with authorization: HTTPClient.Authorization) throws -> HTTPClient.HTTPRequest
  public func request() throws -> HTTPClient.HTTPRequest
  public func request(with authorizationHeaders: [HTTPClient.Header : Swift.String]) throws -> HTTPClient.HTTPRequest
  public func request(with authorizationParams: [HTTPClient.Parameter]) throws -> HTTPClient.HTTPRequest
}
@_hasMissingDesignatedInitializers final public class URLSessionRequestManager : HTTPClient.RequestManager {
  public struct Options : Swift.OptionSet {
    public static let writeResponsesToDocuments: HTTPClient.URLSessionRequestManager.Options
    public static let httpsOnly: HTTPClient.URLSessionRequestManager.Options
    public var rawValue: Swift.Int
    public init(rawValue: Swift.Int)
    public typealias Element = HTTPClient.URLSessionRequestManager.Options
    public typealias ArrayLiteralElement = HTTPClient.URLSessionRequestManager.Options
    public typealias RawValue = Swift.Int
  }
  convenience public init(policies: HTTPClient.ServerTrustPolicies = [:], options: HTTPClient.URLSessionRequestManager.Options = [])
  final public func invalidate()
  final public func clearURLCache()
  final public func performRequest(_ request: HTTPClient.HTTPRequest, completionQueue: Dispatch.DispatchQueue, completion: @escaping (Swift.Result<HTTPClient.HTTPResponse, HTTPClient.HTTPError>) -> Swift.Void)
  final public func performDownload(_ request: HTTPClient.HTTPRequest, setter: HTTPClient.DownloadTask.Setter, completionQueue: Dispatch.DispatchQueue, completion: @escaping (Swift.Result<HTTPClient.DownloadResult, HTTPClient.HTTPError>) -> Swift.Void)
  @objc deinit
}
public protocol EmptyResultPayload {
  init()
}
public struct EmptyPayload : HTTPClient.EmptyResultPayload {
  public init()
}
extension Request where Self.ResultPayloadType : HTTPClient.EmptyResultPayload {
  public func resultPayload(from response: HTTPClient.HTTPResponse) throws -> Self.ResultPayloadType
}
public protocol MockRequestHandler {
  func response(for request: HTTPClient.HTTPRequest) -> HTTPClient.HTTPResponse?
}
public struct MockRequestManager : HTTPClient.RequestManager {
  public init(mockHandlers: [HTTPClient.MockRequestHandler], delay: Swift.Double = 0.0)
  public func invalidate()
  public func performRequest(_ request: HTTPClient.HTTPRequest, completionQueue: Dispatch.DispatchQueue, completion: @escaping (Swift.Result<HTTPClient.HTTPResponse, HTTPClient.HTTPError>) -> Swift.Void)
  public func performDownload(_ request: HTTPClient.HTTPRequest, setter: HTTPClient.DownloadTask.Setter, completionQueue: Dispatch.DispatchQueue, completion: @escaping (Swift.Result<HTTPClient.DownloadResult, HTTPClient.HTTPError>) -> Swift.Void)
}
public struct Header : Swift.RawRepresentable, Swift.Equatable, Swift.Hashable, Swift.CustomStringConvertible {
  public typealias RawValue = Swift.String
  public var rawValue: HTTPClient.Header.RawValue
  public init(_ rawValue: HTTPClient.Header.RawValue)
  public init(rawValue: HTTPClient.Header.RawValue)
  public var description: Swift.String {
    get
  }
  public static let acceptCharset: HTTPClient.Header
  public static let acceptEncoding: HTTPClient.Header
  public static let authorization: HTTPClient.Header
  public static let accept: HTTPClient.Header
  public static let acceptLanguage: HTTPClient.Header
  public static let contentType: HTTPClient.Header
  public static let contentLength: HTTPClient.Header
  public static let contentDisposition: HTTPClient.Header
  public static let contentTransferEncoding: HTTPClient.Header
  public static let userAgent: HTTPClient.Header
  public static let location: HTTPClient.Header
  public static let ifNoneMatch: HTTPClient.Header
  public static let eTag: HTTPClient.Header
  public static let cacheControl: HTTPClient.Header
  public static let expires: HTTPClient.Header
  public static let age: HTTPClient.Header
  public static let date: HTTPClient.Header
}
public struct HTTPRequest {
  public struct Method : Swift.Hashable {
    public typealias RawValue = Swift.String
    public var rawValue: HTTPClient.HTTPRequest.Method.RawValue
    public init(_ rawValue: HTTPClient.HTTPRequest.Method.RawValue)
    public init(rawValue: HTTPClient.HTTPRequest.Method.RawValue)
    public static let get: HTTPClient.HTTPRequest.Method
    public static let post: HTTPClient.HTTPRequest.Method
    public static let put: HTTPClient.HTTPRequest.Method
    public static let delete: HTTPClient.HTTPRequest.Method
    public static let head: HTTPClient.HTTPRequest.Method
    public static let patch: HTTPClient.HTTPRequest.Method
    public static let options: HTTPClient.HTTPRequest.Method
    public static let connect: HTTPClient.HTTPRequest.Method
    public static let trace: HTTPClient.HTTPRequest.Method
    public var hashValue: Swift.Int {
      get
    }
    public func hash(into hasher: inout Swift.Hasher)
    public static func == (a: HTTPClient.HTTPRequest.Method, b: HTTPClient.HTTPRequest.Method) -> Swift.Bool
  }
  public let method: HTTPClient.HTTPRequest.Method
  public let baseURL: Foundation.URL
  public let endpoint: Swift.String
  public let parameters: [HTTPClient.Parameter]?
  public let headers: [HTTPClient.Header : Swift.String]
  public let body: HTTPClient.HTTPRequestBody?
  public let acceptedContentTypes: Swift.Set<HTTPClient.ContentType>
  public let acceptedCharset: HTTPClient.Charset
  public let containsSensitiveInformation: Swift.Bool
  public let shouldFollowRedirects: Swift.Bool
  public let reuseAuthorizationHeader: Swift.Bool
  public let uuid: Foundation.UUID
  public init(_ method: HTTPClient.HTTPRequest.Method, baseURL: Foundation.URL, endpoint: Swift.String, parameters: [HTTPClient.Parameter]? = nil, headers: [HTTPClient.Header : Swift.String] = [ : ], body: HTTPClient.HTTPRequestBody? = nil, acceptedContentTypes: Swift.Set<HTTPClient.ContentType> = [.applicationJSON], acceptedCharset: HTTPClient.Charset = .utf8, containsSensitiveInformation: Swift.Bool = false, shouldFollowRedirects: Swift.Bool = true, reuseAuthorizationHeader: Swift.Bool = false)
  public var urlFormEncodedBody: Swift.String? {
    get
  }
  public func url() throws -> Foundation.URL
}
extension HTTPRequest : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
public class DownloadedFile {
  public var url: Foundation.URL {
    get
  }
  public var isTemporary: Swift.Bool {
    get
  }
  public init(url: Foundation.URL)
  @objc deinit
  @available(*, deprecated, message: "Will be removed in the next major release.")
  public func load() throws -> Foundation.Data
  @available(*, deprecated, message: "Will be removed in the next major release.")
  public func rename(to newName: Swift.String) throws
  @available(*, deprecated, message: "Will be removed in the next major release.")
  public func makeTemporary() throws
  public func move(to location: Foundation.URL) throws
  @available(*, deprecated, message: "Will be removed in the next major release.")
  public func copy(to location: Foundation.URL) throws
}
public typealias ServerTrustPolicies = [Swift.String : HTTPClient.ServerTrustPolicy]
extension Dictionary where Key == Swift.String, Value == HTTPClient.ServerTrustPolicy {
  public func handle(challenge: Foundation.URLAuthenticationChallenge, completionHandler: @escaping (Foundation.URLSession.AuthChallengeDisposition, Foundation.URLCredential?) -> Swift.Void)
}
indirect public enum ServerTrustPolicy {
  public static let anyHost: Swift.String
  public enum VerifyRoot {
    case sha1Fingerprint(Swift.String)
    case sha256Fingerprint(Swift.String)
    case rootCA(Security.SecCertificate)
  }
  public typealias CredentialProvider = () -> Foundation.URLCredential?
  case disableEvaluation
  case blacklist
  case verifyRoot([HTTPClient.ServerTrustPolicy.VerifyRoot])
  case authenticationByURLCredential(HTTPClient.ServerTrustPolicy.CredentialProvider, verifyRoot: [HTTPClient.ServerTrustPolicy.VerifyRoot]?)
  public func evaluate(trust: Security.SecTrust, for host: Swift.String) -> Swift.Bool
  public func evaluate(challenge: Foundation.URLAuthenticationChallenge) -> (Foundation.URLSession.AuthChallengeDisposition, Foundation.URLCredential?)
}
extension ServerTrustPolicy.VerifyRoot {
  public func evaluate(trust: Security.SecTrust, for host: Swift.String) -> Swift.Bool
}
public protocol RequestErrorParser {
  associatedtype ErrorPayloadType
  associatedtype ConnectorErrorType : HTTPClient.ConnectorError
  func errorPayload(from response: HTTPClient.HTTPResponse) throws -> Self.ErrorPayloadType
  func error(from errorPayload: Self.ErrorPayloadType) -> Self.ConnectorErrorType?
  var uniqueRequestIdentifier: HTTPClient.UniqueRequestIdentifier { get }
}
extension RequestErrorParser {
  public func error(from errorPayload: Self.ErrorPayloadType) -> Self.ConnectorErrorType?
  public var uniqueRequestIdentifier: HTTPClient.UniqueRequestIdentifier {
    get
  }
}
public protocol ConnectorError : Swift.Error {
  static func error(from httpError: HTTPClient.HTTPError, for requestIdentifier: HTTPClient.UniqueRequestIdentifier) -> Self
}
public struct UniqueRequestIdentifier : Swift.RawRepresentable, Swift.Equatable {
  public typealias RawValue = Swift.String
  public let rawValue: HTTPClient.UniqueRequestIdentifier.RawValue
  public init(rawValue: HTTPClient.UniqueRequestIdentifier.RawValue)
  public init(_ rawValue: HTTPClient.UniqueRequestIdentifier.RawValue)
}
extension UniqueRequestIdentifier : Swift.ExpressibleByStringLiteral {
  public init(stringLiteral value: Swift.String)
  public typealias StringLiteralType = Swift.String
  public typealias ExtendedGraphemeClusterLiteralType = Swift.String
  public typealias UnicodeScalarLiteralType = Swift.String
}
extension UniqueRequestIdentifier : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
public protocol HTTPRequestBody {
  var contentType: HTTPClient.ContentType { get }
  func data() throws -> Foundation.Data
}
extension JSON : HTTPClient.HTTPRequestBody {
  public var contentType: HTTPClient.ContentType {
    get
  }
  public func data() throws -> Foundation.Data
}
public struct StatusCode : Swift.RawRepresentable, Swift.Hashable, Swift.CustomStringConvertible {
  public typealias RawValue = Swift.Int
  public var rawValue: HTTPClient.StatusCode.RawValue
  public init(_ rawValue: HTTPClient.StatusCode.RawValue)
  public init(rawValue: HTTPClient.StatusCode.RawValue)
  public var description: Swift.String {
    get
  }
  public static let `continue`: HTTPClient.StatusCode
  public static let switchingProtocols: HTTPClient.StatusCode
  public static let processing: HTTPClient.StatusCode
  public static let earlyHints: HTTPClient.StatusCode
  public static let ok: HTTPClient.StatusCode
  public static let created: HTTPClient.StatusCode
  public static let accepted: HTTPClient.StatusCode
  public static let nonAuthoritativeInformation: HTTPClient.StatusCode
  public static let noContent: HTTPClient.StatusCode
  public static let resetContent: HTTPClient.StatusCode
  public static let partialContent: HTTPClient.StatusCode
  public static let multiStatus: HTTPClient.StatusCode
  public static let alreadyReported: HTTPClient.StatusCode
  public static let IMUsed: HTTPClient.StatusCode
  public static let multipleChoices: HTTPClient.StatusCode
  public static let movedPermanently: HTTPClient.StatusCode
  public static let found: HTTPClient.StatusCode
  public static let seeOther: HTTPClient.StatusCode
  public static let notModified: HTTPClient.StatusCode
  public static let useProxy: HTTPClient.StatusCode
  public static let switchProxy: HTTPClient.StatusCode
  public static let temporaryRedirect: HTTPClient.StatusCode
  public static let permanentRedirect: HTTPClient.StatusCode
  public static let badRequest: HTTPClient.StatusCode
  public static let unauthorized: HTTPClient.StatusCode
  public static let paymentRequired: HTTPClient.StatusCode
  public static let forbidden: HTTPClient.StatusCode
  public static let notFound: HTTPClient.StatusCode
  public static let methodNotAllowed: HTTPClient.StatusCode
  public static let notAcceptable: HTTPClient.StatusCode
  public static let proxyAuthenticationRequired: HTTPClient.StatusCode
  public static let requestTimeout: HTTPClient.StatusCode
  public static let conflict: HTTPClient.StatusCode
  public static let gone: HTTPClient.StatusCode
  public static let lengthRequired: HTTPClient.StatusCode
  public static let preconditionFailed: HTTPClient.StatusCode
  public static let PayloadTooLarge: HTTPClient.StatusCode
  public static let URITooLong: HTTPClient.StatusCode
  public static let unsupportedMediaType: HTTPClient.StatusCode
  public static let rangeNotSatisfiable: HTTPClient.StatusCode
  public static let expectationFailed: HTTPClient.StatusCode
  public static let ImATeapot: HTTPClient.StatusCode
  public static let misdirectedRequest: HTTPClient.StatusCode
  public static let unprocessableEntity: HTTPClient.StatusCode
  public static let locked: HTTPClient.StatusCode
  public static let failedDependency: HTTPClient.StatusCode
  public static let upgradeRequired: HTTPClient.StatusCode
  public static let preconditionRequired: HTTPClient.StatusCode
  public static let tooManyRequests: HTTPClient.StatusCode
  public static let requestHeaderFieldsTooLarge: HTTPClient.StatusCode
  public static let unavailableForLegalReasons: HTTPClient.StatusCode
  public static let internalServerError: HTTPClient.StatusCode
  public static let notImplemented: HTTPClient.StatusCode
  public static let badGateway: HTTPClient.StatusCode
  public static let serviceUnavailable: HTTPClient.StatusCode
  public static let gatewayTimeout: HTTPClient.StatusCode
  public static let HTTPVersionNotSupported: HTTPClient.StatusCode
  public static let variantAlsoNegotiates: HTTPClient.StatusCode
  public static let insufficientStorage: HTTPClient.StatusCode
  public static let loopDetected: HTTPClient.StatusCode
  public static let notExtended: HTTPClient.StatusCode
  public static let networkAuthenticationRequired: HTTPClient.StatusCode
}
public struct JSONServerError : HTTPClient.DefaultErrorPayload {
  public let statusCode: HTTPClient.StatusCode
  public let responseHeaders: [HTTPClient.Header : Swift.String]
  public let json: Utility.JSON
  public init(response: HTTPClient.HTTPResponse) throws
}
public protocol RequestManager {
  func performRequest(_ request: HTTPClient.HTTPRequest, completionQueue: Dispatch.DispatchQueue, completion: @escaping (Swift.Result<HTTPClient.HTTPResponse, HTTPClient.HTTPError>) -> Swift.Void)
  func performDownload(_ request: HTTPClient.HTTPRequest, setter: HTTPClient.DownloadTask.Setter, completionQueue: Dispatch.DispatchQueue, completion: @escaping (Swift.Result<HTTPClient.DownloadResult, HTTPClient.HTTPError>) -> Swift.Void)
  func invalidate()
}
@_hasMissingDesignatedInitializers public class ClientErrorObservation {
  public typealias Handler = (Swift.Error) -> Swift.Void
  @objc deinit
}
final public class Client {
  public init(requestManager: HTTPClient.RequestManager)
  @objc deinit
  final public func registerErrorObserver(_ handler: @escaping HTTPClient.ClientErrorObservation.Handler) -> HTTPClient.ClientErrorObservation
  final public func performRequest<RequestType>(_ request: RequestType, completionQueue: Dispatch.DispatchQueue? = nil, completion: @escaping (Swift.Result<RequestType.ResultPayloadType, RequestType.ConnectorErrorType>) -> Swift.Void) where RequestType : HTTPClient.Request
  @discardableResult
  final public func performDownload<RequestType>(_ request: RequestType, completionQueue: Dispatch.DispatchQueue? = nil, completion: @escaping (Swift.Result<RequestType.ResultPayloadType, RequestType.ConnectorErrorType>) -> Swift.Void) -> HTTPClient.DownloadTask where RequestType : HTTPClient.DownloadRequest
}
public struct Charset : Swift.RawRepresentable, Swift.Equatable, Swift.Hashable, Swift.CustomStringConvertible {
  public typealias RawValue = Swift.String
  public var rawValue: HTTPClient.Charset.RawValue
  public init(_ rawValue: HTTPClient.Charset.RawValue)
  public init(rawValue: HTTPClient.Charset.RawValue)
  public var description: Swift.String {
    get
  }
  public static let iso88595: HTTPClient.Charset
  public static let utf8: HTTPClient.Charset
  public static let utf16: HTTPClient.Charset
}
public struct OAuthServerError : HTTPClient.DefaultErrorPayload, Swift.Equatable {
  public let statusCode: HTTPClient.StatusCode
  public let error: Swift.String
  public let errorDescription: Swift.String?
  public init(response: HTTPClient.HTTPResponse) throws
  public static func == (a: HTTPClient.OAuthServerError, b: HTTPClient.OAuthServerError) -> Swift.Bool
}
public struct Parameter : Swift.Equatable {
  public let field: Swift.String
  public let value: Swift.String?
  public init(field: Swift.String, value: Swift.String?)
  public static func == (a: HTTPClient.Parameter, b: HTTPClient.Parameter) -> Swift.Bool
}
extension Array : Swift.ExpressibleByDictionaryLiteral where Element == HTTPClient.Parameter {
  public typealias Key = Swift.String
  public typealias Value = Swift.String?
  public init(dictionaryLiteral elements: (Swift.String, Swift.String?)...)
}
extension HTTPResponse {
  public init?(for requestURL: Foundation.URL, withContentsOf fileURL: Foundation.URL)
}
public struct MockFileRequestHandler : HTTPClient.MockRequestHandler {
  public init(mockFilesURL: Foundation.URL, handleETag: Swift.Bool = false, requestInterceptor: ((HTTPClient.HTTPRequest) -> HTTPClient.HTTPRequest)? = nil)
  public func response(for request: HTTPClient.HTTPRequest) -> HTTPClient.HTTPResponse?
  public static func filename(for request: HTTPClient.HTTPRequest, includeQuery: Swift.Bool = true) -> Swift.String
}
public enum HTTPError : Swift.Error {
  case invalidRequest(Swift.String)
  case invalidResponse(Swift.String)
  case requestSerializationError(Swift.Error)
  case networkError(Swift.Error)
  case responseDeserializationError(Swift.Error)
  case unrecognizedResponse(HTTPClient.HTTPResponse)
  case downloadCancelled
  case clientInvalidated
  case unexpectedAuthorization
  case invalidJWTTokenFormat(Swift.String)
  case authorizationError(Swift.Error)
}
public struct HTTPResponse {
  public let url: Foundation.URL
  public let statusCode: HTTPClient.StatusCode
  public let headers: [HTTPClient.Header : Swift.String]
  public let body: Foundation.Data?
  public init(url: Foundation.URL, statusCode: HTTPClient.StatusCode, headers: [HTTPClient.Header : Swift.String], body: Foundation.Data?)
}
extension HTTPResponse : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
public protocol DownloadRequest : HTTPClient.RequestBuilder, HTTPClient.RequestErrorParser {
  associatedtype ResultPayloadType
  func resultPayload(from response: HTTPClient.HTTPResponse, downloaded file: HTTPClient.DownloadedFile) throws -> Self.ResultPayloadType
}
extension DownloadRequest where Self.ResultPayloadType == HTTPClient.DownloadedFile {
  public func resultPayload(from response: HTTPClient.HTTPResponse, downloaded file: HTTPClient.DownloadedFile) throws -> HTTPClient.DownloadedFile
}
extension HTTPResponse {
  public func jsonBody() throws -> Utility.JSON
}
extension Request where Self.ResultPayloadType : Utility.JSONDecodable {
  public func resultPayload(from response: HTTPClient.HTTPResponse) throws -> Self.ResultPayloadType
}
public struct JWT : Swift.Equatable {
  public let header: Swift.String
  public let payload: Utility.JSON
  public let signature: Swift.String
  public init(string: Swift.String) throws
  public static func == (a: HTTPClient.JWT, b: HTTPClient.JWT) -> Swift.Bool
}
public struct AccessToken {
  public let token: Swift.String
  public let expirationDate: Foundation.Date
  public let scope: Swift.String?
  public init(token: Swift.String, expirationDate: Foundation.Date, scope: Swift.String?)
  public func stillValid(forSeconds seconds: Foundation.TimeInterval) -> Swift.Bool
}
extension AccessToken : Utility.JSONDecodable {
  public init(json: Utility.JSON) throws
}
public protocol Request : HTTPClient.RequestBuilder, HTTPClient.RequestErrorParser {
  associatedtype ResultPayloadType
  func resultPayload(from response: HTTPClient.HTTPResponse) throws -> Self.ResultPayloadType
}
public struct DownloadResult {
  public let response: HTTPClient.HTTPResponse
  public let file: Foundation.URL
  public init(response: HTTPClient.HTTPResponse, file: Foundation.URL)
}
public enum DownloadState {
  case waiting
  case inProgress(bytes: Swift.Int64, total: Swift.Int64?)
  case cancelled
  case loaded
  case failed
}
public protocol DownloadTaskObserver {
  func downloadTask(_ task: HTTPClient.DownloadTask, didUpdateWith state: HTTPClient.DownloadState)
}
@_hasMissingDesignatedInitializers public class DownloadTask {
  public var state: HTTPClient.DownloadState {
    get
  }
  public func cancel()
  public func add(observer: HTTPClient.DownloadTaskObserver)
  public func remove(observer: HTTPClient.DownloadTaskObserver)
  @objc deinit
}
extension DownloadTask {
  @_hasMissingDesignatedInitializers public class Setter {
    public typealias CancelHandler = () -> Swift.Void
    public var onCancel: HTTPClient.DownloadTask.Setter.CancelHandler? {
      get
      set
    }
    public func didWrite(bytes: Swift.Int64, total: Swift.Int64?)
    @objc deinit
  }
}
public protocol Authorized {
  var authorizationProvider: HTTPClient.AuthorizationProvider { get }
}
public typealias AuthorizedRequest = HTTPClient.Authorized & HTTPClient.Request
public typealias AuthorizedDownloadRequest = HTTPClient.Authorized & HTTPClient.DownloadRequest
public protocol AuthorizationProvider {
  func authorization(completion: @escaping (HTTPClient.AuthorizationProviderResult) -> Swift.Void)
}
public enum AuthorizationProviderResult {
  case authorized(HTTPClient.Authorization)
  case error(Swift.Error)
}
public enum Authorization {
  case headers([HTTPClient.Header : Swift.String])
  case params([HTTPClient.Parameter])
  case none
}
public struct TransferEncoding : Swift.RawRepresentable, Swift.Equatable, Swift.Hashable {
  public typealias RawValue = Swift.String
  public var rawValue: HTTPClient.TransferEncoding.RawValue
  public init(_ rawValue: HTTPClient.TransferEncoding.RawValue)
  public init(rawValue: HTTPClient.TransferEncoding.RawValue)
  public var description: Swift.String {
    get
  }
  public static let sevenBit: HTTPClient.TransferEncoding
  public static let eightBit: HTTPClient.TransferEncoding
  public static let base64: HTTPClient.TransferEncoding
  public static let binary: HTTPClient.TransferEncoding
  public static let quotedPrintable: HTTPClient.TransferEncoding
}
public struct ContentType : Swift.RawRepresentable, Swift.Equatable, Swift.Hashable, Swift.CustomStringConvertible {
  public typealias RawValue = Swift.String
  public var rawValue: HTTPClient.ContentType.RawValue
  public init(_ rawValue: HTTPClient.ContentType.RawValue)
  public init(rawValue: HTTPClient.ContentType.RawValue)
  public var description: Swift.String {
    get
  }
  public static let applicationXWwwFormURLEncoded: HTTPClient.ContentType
  public static let applicationJSON: HTTPClient.ContentType
  public static let applicationPDF: HTTPClient.ContentType
  public static let imagePNG: HTTPClient.ContentType
  public static let imageJPEG: HTTPClient.ContentType
  public static let imageGIF: HTTPClient.ContentType
  public static let textPlain: HTTPClient.ContentType
  public static let textHTML: HTTPClient.ContentType
}
public struct MultipartFormData {
  public enum Error : Swift.Error {
    case stringConvertible(Swift.String, HTTPClient.TransferEncoding)
  }
  public struct File : Swift.Equatable {
    public let data: Foundation.Data
    public let filename: Swift.String
    public let mimeType: HTTPClient.ContentType
    public init(data: Foundation.Data, filename: Swift.String, mimeType: HTTPClient.ContentType)
    public static func == (a: HTTPClient.MultipartFormData.File, b: HTTPClient.MultipartFormData.File) -> Swift.Bool
  }
  public struct Parameter : Swift.Equatable {
    public let value: Swift.String
    public let encoding: HTTPClient.TransferEncoding
    public init(value: Swift.String, encoding: HTTPClient.TransferEncoding)
    public static func == (a: HTTPClient.MultipartFormData.Parameter, b: HTTPClient.MultipartFormData.Parameter) -> Swift.Bool
  }
  public struct Part {
    public let name: Swift.String
    public let value: HTTPClient.MultipartFormDataPart
  }
  public var parts: [HTTPClient.MultipartFormData.Part] {
    get
  }
  public init()
}
public protocol MultipartFormDataPart {
  func partHeaders(partName: Swift.String) -> [HTTPClient.Header : Swift.String]
  func partData() throws -> Foundation.Data
}
extension MultipartFormData.File : HTTPClient.MultipartFormDataPart {
  public func partHeaders(partName: Swift.String) -> [HTTPClient.Header : Swift.String]
  public func partData() throws -> Foundation.Data
}
extension MultipartFormData.Parameter : HTTPClient.MultipartFormDataPart {
  public func partHeaders(partName: Swift.String) -> [HTTPClient.Header : Swift.String]
  public func partData() throws -> Foundation.Data
}
extension MultipartFormData {
  public mutating func add(part: HTTPClient.MultipartFormDataPart, name: Swift.String)
  public mutating func removePart(withName name: Swift.String)
  public func part(withName name: Swift.String) -> HTTPClient.MultipartFormDataPart?
}
extension MultipartFormData : HTTPClient.HTTPRequestBody {
  public var contentType: HTTPClient.ContentType {
    get
  }
  public func data() throws -> Foundation.Data
}
public protocol DefaultErrorPayload {
  init(response: HTTPClient.HTTPResponse) throws
}
extension RequestErrorParser where Self.ErrorPayloadType : HTTPClient.DefaultErrorPayload {
  public func errorPayload(from response: HTTPClient.HTTPResponse) throws -> Self.ErrorPayloadType
}
