
@import Foundation;
#import <LicenseKit/AAFLicense.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, AAFLicenseManagerAEVLogoStyle)
{
    AAFLicenseManagerAEVLogoStyleDark = 0,
    AAFLicenseManagerAEVLogoStyleLight,
};

@interface AAFLicenseManager : NSObject

/**
 Property to style the webview via CSS
 
 With this method the CSS default of the webview can be overwritten.
 
 Reset CSS to default with method resetCSS
 
 Example-Code to read and pass Custom.css file in the implementing project:
 [AAFLicenseManager sharedLicenseManager].CSS = [NSString stringWithContentsOfFile:[[NSBundle bundleForClass:[self class]] pathForResource:@"Custom"
                                                                                                                                    ofType:@"css"]
                                                                          encoding:NSUTF8StringEncoding
                                                                             error:&error];
 as an alternative CSS can also be passed directly as string.
 
     body {
        background-color: black;
         text-align: center;
         color: white;
         font-family: "AudiTypeDisplay-Normal";
     }
     div.license_title {
        text-align: center;
     }
     div.license_body {
        text-align: justify;
     }
     a:link, a:visited, a:focus, a:hover {
         color: white;
         text-decoration: none;
     }
     a:active {
         color: white;
         text-decoration: underline;
     }
     img.logo {
        width: 178pt;
     }

    No use a custom font shipped with your application, include a font-face declaration per font at the beginning of your CSS file or string.

     @font-face {
         font-family: '<Name of your font>';
         src: url('<Path to your font relative to the app bundle>') format('<format of your font>');
     }
 */
@property (nonatomic, copy, readwrite, null_resettable) NSString *CSS;
@property (nonatomic, copy, readwrite, null_resettable) NSString *headerHTML;
@property (nonatomic, copy, readwrite, null_resettable) NSString *footerHTML;

/**
 Property to set a custom application name
 
 The application name is displayed in the headline of each framework. 
 
 When no custom application name is set the default string "This application" is used
 Default: 'This application uses FRAMEWORK'
 Example (App-Name): 'App-Name uses FRAMEWORK'
 */
@property (nonatomic, copy, readwrite, null_resettable) NSString *applicationName;

/**
 Change this property to show or hide the Quartett mobile logo and company information. Default: NO.
 */
@property (nonatomic, assign, readwrite, getter=isQuartettMobileInformationHidden) BOOL quartettMobileInformationHidden;

/**
 Change this property to show or hide the AEV logo and company information. Default: YES.
 */
@property (nonatomic, assign, readwrite, getter=isAEVInformationHidden) BOOL aevInformationHidden;

/**
 Change this property to show different styles of the AEV logo. Default: AAFLicenseManagerAEVLogoStyleDark
 */
@property (nonatomic, assign, readwrite) AAFLicenseManagerAEVLogoStyle aevLogoStyle;

/**
 This returns the generated HTML data
 */
@property (nonatomic, copy, readonly) NSData *licensesHTMLData;

/**
 This framework should be used to collect licenses from different frameworks and/or classes. The collected licenses are then displayed in an UIView the framework provides.
 */

/**
 Singleton which enables to add licenses and to get a view which displays all registered licenses.
 */
+ (AAFLicenseManager *)sharedLicenseManager;

/**
 Method to register a license provider
 
 With this method a license provider can be registered.
 
 @param  licenseProvider Register a class that fullfils the AAFLicenseProvider protocol
 */
- (void)registerLicenseProvider:(Class<AAFLicenseProvider>)licenseProvider;

/**
 Method to generate dump file that is stored into the Documents folder of the app bundle
 */
- (void)dumpLicensesToFile;

@end

NS_ASSUME_NONNULL_END
