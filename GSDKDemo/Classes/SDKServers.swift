//
//  SDKServers.swift
//  GSDKDemo
//
//  Created by KingYoung on 2020/05/20.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import Foundation

enum Services: String, CaseIterable {
    case MBBLogin
    case MBBLogout
    case CarFinder
    case RemoteHonkAndFlash
    case VehicleStatusReport
    case RemoteLockUnlock
    case RemoteHeating
    case ValetAlert
    case SpeedAlert
    case Geofencing
    case RemoteBatteryCharge
    case RemotePretripClimatisation
    case RemoteDepartureTime
    case RemoteTripStatistics
    case DestinationImport
    case TheftAlarm
    case VehicleTracking
    case AssistanceCall
    case ServiceAppointment
    case RemoteProfileTimer
    case ETronRoutePlanning
    case PlugAndCharge
    
    
    enum CarFinderService: String, CaseIterable {
        case requestCurrentLocation
    }
    
    enum RemoteHonkAndFlashService: String, CaseIterable {
        case performServiceRequest
        case loadHistory
    }
    
    enum VehicleStatusReportService: String, CaseIterable {
        case requestCurrentStatusReport
    }
    
    enum RemoteLockUnlockService: String, CaseIterable {
        case lock
        case unlock
        case loadLastVehicleActions
    }
    
    enum RemoteHeatingService: String, CaseIterable {
        case sendTimers
        case quickStart
        case quickStop
    }
    
    enum ValetAlertService: String, CaseIterable {
        case createDefinition
        case deleteDefinition
        case reloadViolations
        case deleteViolations
    }
    
    enum SpeedAlertService: String, CaseIterable {
        case sendDefinitions
        case reloadViolations
        case deleteViolations
    }
    
    enum GeofencingService: String, CaseIterable {
        case sendDefinitions
        case reloadViolations
        case deleteViolations
    }
    
    enum RemoteBatteryChargeService: String, CaseIterable {
        case startCharging
        case stopCharging
        case resetChargerSettings
        case requestCurrentChargerSettings
        case selectChargeMode
        case allowAutomaticPlugUnlock
    }
    
    enum RemotePretripClimatisationService: String, CaseIterable {
        case startClimatisation
        case stopClimatisation
        case startWindowHeating
        case stopWindowHeating
        case setClimaterSettings
        case resetClimaterSettings
        case requestCurrentClimaterSettings
    }
    
    enum RemoteDepartureTimeService: String, CaseIterable {
        case setProfiles
        case resetSettings
        case requestCurrentSettings
    }
    
    enum RemoteTripStatisticsService: String, CaseIterable {
        case reloadNewestTrip
        case deleteAllTrips
        case reloadTripsReport
    }
    
    enum DestinationImportService: String, CaseIterable {
        case addDestination
    }
    
    enum TheftAlarmService: String, CaseIterable {
        case reloadAlertHistory
        case deleteAlertHistory
        case loadHistoryActivated
        case activateHistory
        case deactivateHistory
    }
    
    enum VehicleTrackingService: String, CaseIterable {
        case loadSecureCenter
        case loadSpecialModes
        case downloadCertificatePDF
        case getCertificateTypes
        case disable
        case set
        case loadSetupStatus
    }
    
    enum AssistanceCallService: String, CaseIterable {
        case loadAssistancePhoneNumber
    }
    
    enum ServiceAppointmentService: String, CaseIterable {
        case set
        case loadConfiguration
        case activate
        case deactivate
    }
    
    enum RemoteProfileTimerService: String, CaseIterable {
        case setTimers
        case setProfiles
        case SetTimersAndProfiles
    }
    
    enum ETronRoutePlanningService: String, CaseIterable {
        case loadConsumptionData
    }
    
    enum PlugAndChargeService: String, CaseIterable {
        case activate
        case deactivate
    }
}
