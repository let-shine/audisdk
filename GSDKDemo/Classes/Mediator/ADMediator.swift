//
//  ADMediator.swift
//  GSDKDemo
//
//  Created by KingYoung on 2020/05/13.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import Foundation
import UIKit
import MBBConnector
import HTTPClient
import Utility

let identityToken = "eyJraWQiOiIwMDAwMyIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJXODUwMjUwNDk4Iiwic2NwIjpbInByb2ZpbGUiLCJtYmIiXSwidmVyIjoiMSIsImFtciI6InB3ZCIsImNvciI6IkNOIiwia2lkIjoiMDAwMDMiLCJpc3MiOiJodHRwczpcL1wvcHJlLWlkLmF1ZGkuY29tXC92MVwvIiwidHlwZSI6ImlkZW50aXR5IiwibG9jYWxlIjoiemgtQ04iLCJhdWQiOlsiVldHTUJCMDFERVBSRTIiLCJWV0dNQkIwMURFUFJFMSIsIlZXR01CQjAxREVBUFAxIiwiVldHTUJCMDFDTkFQUDEiLCJWV0dNQkIwMUNOUFJFMSIsIlZXR01CQjAxQ05QUkwxIiwibW1pY29ubmVjdF9pb3MiXSwiYXpwIjoibW1pY29ubmVjdF9pb3MiLCJleHAiOjE1ODk0NDA0NDUsImlhdCI6MTU4OTQzOTg0NSwianRpIjoiMDE1ZDY1OTMtYzc5MS00YTJiLTlkYTYtZDY0Nzc1MGMzYjBlIn0.WfGKv_2elSGnLc6uHia7Rs7movEBHPZPlBUR_PYjyLRvF3DW5yabYfU6YN9USyiczAfmxB215cywrB5eodOt-lBGWBpbVUEdpiH_wpQGxlHc55cmeEcO60MRfkZHBZ_Bta8BEhRsW1SbQqSHlQ8aEWjvEfpLerohcvsDyWGGzzkJpFr37VpH7J1__Q4psIIxM7FCA_NRqf_wBZV9JOa-1ABgC_JB2cfIU5e8TC-n3vYwtWNbt1dAm6RffPaCmrRJbqz1NNMCMvvGxFhl77T2G_s1BothixEEhya_eWG6jlZPoVThOmR2xbwTuEN2fHVBdn4RMI--t2WuOnz8c743Xw"
let vin = "BAUNEE4K020050601"

//let falBaseURL = URL(string: "https://msg.audi.de/fs-car/")!
//let malBaseURL = URL(string: "https://mal-1a.prd.ece.vwg-connect.com/api/")!
//let oAuthURL = URL(string: "https://mbboauth-1d.prd.ece.vwg-connect.com/mbbcoauth")!

let oAuthURL = URL(string: "https://mbboauth-1d.app.cn.vwg-connect.cn/mbbcoauth")!
let falBaseURL = URL(string: "https://pre-msg.audi-connect.cn/fs-car-ng/")!
let malBaseURL = URL(string: "https://mal-1a.app.cn.vwg-connect.cn/api/")!

let latitude = 31.40527
let longitude = 121.48941

enum Req {
    enum GSDK {
        case login
        case logout
    }


}




//enum APIError : ConnectorError {
//
//    case http(HTTPError)
//
//    static func error(from httpError: HTTPError, for requestIdentifier: UniqueRequestIdentifier) -> APIError {
//        return .http(httpError)
//    }
//}



class ADMediator {
    
    static let shared: ADMediator = {
        ADMediator.registerGSDKActions()
        
        return ADMediator()
    }()
    
    lazy var mbb: Connector = {

          let client = Client(requestManager: URLSessionRequestManager())
          let app = App(brand: .audi, name: "MBBDemo", version: "1.0", id: Bundle.main.bundleIdentifier!, pushAppId: nil, platform: .apple)
      
          return Connector(client: client,
                           app: app,
                           operationListVersion: .v3,
                           falBaseURL: falBaseURL,
                           malBaseURL: malBaseURL,
                           oAuthURL: oAuthURL,
                           keychainTokenPrefix: "MBBConnector",
                           stageIdentifier: "Test",
                           targetSecurePINVersion: .v2,
                           useInvocationURLs: false,
                           persistencyDelegate: nil,
                           persistedData: nil) {
              
                  }
     }()
    
    var vehicle: Vehicle?

    static func request(withKey key: AnyHashable, userInfo: [String: Any]?, completion: ((_ result: RespResult)->())?) {
        ADMediator.shared.request(withKey: key, userInfo: userInfo, completion: completion)
    }
    
    private func request(withKey key: AnyHashable, userInfo: [String: Any]?, completion: ((_ result: RespResult)->())?) {
        ADRequester.request(withKey: key, userInfo: userInfo) { result in
            DispatchQueue.main.async {
                completion?(result)
            }
        }
    }
}

typealias RegisterGSDK = ADMediator
extension RegisterGSDK {
    fileprivate static func registerGSDKActions() {
        
        //登录
        ADRequester.register(withKey: Req.GSDK.login) { (userInfo) -> RespResult in
            let semahpore = DispatchSemaphore(value: 0)
            var res:RespResult?
            
            ADMediator.shared.mbb.authenticate(with: identityToken, clientName: UIDevice.current.name) { error in
                if let error = error {
                    res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                } else {
                    ADMediator.shared.vehicle = Vehicle(vin: vin, connector: ADMediator.shared.mbb)
                    res = RespResult.success("Success")
                    
                }
                semahpore.signal()
            }
            semahpore.wait()
            return res!
        }
        
        ADRequester.register(withKey: Req.GSDK.logout) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            let semahpore = DispatchSemaphore(value: 0)
            var res:RespResult?
            ADMediator.shared.mbb.pnsDeregisterForPushNotifications { (error) in
                if let error = error {
                    // Deregistration FAILED!
                    // Check deregistrationError for the error that occurred

                    // It is probably the correct behavior to just continue with the logout flow.
                }

                ADMediator.shared.mbb.revokeToken { revocationError in
                    if let error = revocationError {
                        // Revocation FAILED!
                        // Check revocationError for the error that occurred

                        // At this point, there is no way to recover from this error, logging the error may still be useful during development
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("success")
                    }
                    semahpore.signal()
                }
            }
            semahpore.wait()
            return res!
        }
        
        
        //carfinder 定位
        ADRequester.register(withKey: Services.CarFinderService.requestCurrentLocation) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.carFinder.isRequestCurrentAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.carFinder.requestCurrentLocation { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.carFinder.report.result!)")
                    }
                    semahpore.signal()
                    
                }
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isRequestCurrentAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteHonkAndFlashService.performServiceRequest) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteHonkAndFlash.isPerformServiceRequestAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteHonkAndFlash.performServiceRequest(RemoteHonkAndFlash.ServiceRequest(actionType: .honkAndFlash, latitude: latitude, longitude: longitude), with: nil) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("RemoteHonkAndFlash Success")
                    }
                    semahpore.signal()
                }
            
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "remoteHonkAndFlash.isPerformServiceRequestAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteHonkAndFlashService.loadHistory) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteHonkAndFlash.isLoadingHistoryAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteHonkAndFlash.loadHistory { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remoteHonkAndFlash.history)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "remoteHonkAndFlash.isLoadingHistoryAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.VehicleStatusReportService.requestCurrentStatusReport) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.vehicleStatusReport.isRequestCurrentVehicleDataAllowed  {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.vehicleStatusReport.requestCurrentStatusReport { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.vehicleStatusReport.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isRequestCurrentVehicleDataAllowed  == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteLockUnlockService.lock) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteLockUnlock.isLockAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteLockUnlock.lock(with: nil) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("success")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isLockAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteLockUnlockService.unlock) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteLockUnlock.isUnlockAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteLockUnlock.unlock(with: nil) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("success")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isUnlockAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteLockUnlockService.loadLastVehicleActions) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteLockUnlock.isLoadLastVehicleActionsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteLockUnlock.loadLastVehicleActions { result in
                    switch result {
                    case .success(let action):
                        res = RespResult.success("\(action)")
                    case .failure(let error):
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    }
                    semahpore.signal()
                    
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isLoadLastVehicleActionsAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteHeatingService.sendTimers) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            
            let semahpore = DispatchSemaphore(value: 0)
            var res:RespResult?
            vehicle.remoteHeating.sendTimers([RemoteHeating.DepartureTimer.TimerId.timer1 : RemoteHeating.DepartureTimer(departureDate: Date(), departureTime: Time(hour: 1, minute: 1)!)], heaterMode: .comfort, securePIN: nil) { (error) in
                if let error = error {
                    res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                } else {
                    res = RespResult.success("success")
                }
                semahpore.signal()
            }
            
            semahpore.wait()
            return res!
        }
        
        
        ADRequester.register(withKey: Services.RemoteHeatingService.quickStart) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteHeating.isQuickStartAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteHeating.quickStart(for: Measurement(value: 1, unit: UnitDuration.minutes), securePIN: nil) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("success")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isQuickStartAllowed == false", data: nil))
            }
        }
            
      
        ADRequester.register(withKey: Services.RemoteHeatingService.quickStop) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteHeating.isQuickStopAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteHeating.quickStop(with: nil) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("success")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isQuickStopAllowed == false", data: nil))
            }
        }
        
        
        ADRequester.register(withKey: Services.ValetAlertService.createDefinition) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.valetAlert.isCreateDefinitionAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.valetAlert.createDefinition(with: Alert.Circle(coordinate: Coordinate(latitude: latitude, longitude: longitude)!, radius: 10), and: Measurement(value: 50, unit: UnitSpeed.kilometersPerHour)) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.valetAlert.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isCreateDefinitionAllowed == false", data: nil))
                
            }
        }
        
        
        ADRequester.register(withKey: Services.ValetAlertService.deleteDefinition) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.valetAlert.isDeleteDefinitionAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.valetAlert.deleteDefinition { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.valetAlert.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
                
                
            } else {
                return RespResult.failure(RespError(code: "", msg: "isDeleteDefinitionAllowed == false", data: nil))
                               
                
            }
            
            
        }
        
        ADRequester.register(withKey: Services.ValetAlertService.reloadViolations) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.valetAlert.isReloadViolationsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                //Result<[ValetAlert.Violation], MBBError>
                vehicle.valetAlert.reloadViolations { result in
                    switch result {
                    case .success(let vio):
                        res = RespResult.success("\(vio)")
                    case .failure(let error):
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                        
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isReloadViolationsAllowed == false", data: nil))
                
            }
            
            
            
        }
        
        ADRequester.register(withKey: Services.ValetAlertService.deleteViolations) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            if vehicle.valetAlert.isDeleteViolationsAllowed {
                
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                
                vehicle.valetAlert.deleteViolations { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.valetAlert.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isDeleteViolationsAllowed == false", data: nil))
            }
            
        }
        
        ADRequester.register(withKey: Services.SpeedAlertService.sendDefinitions) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.speedAlert.isSendDefinitionsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.speedAlert.sendDefinitions([SpeedAlert.Definition.UserDefined(enabled: true, speedLimit: Measurement(value: 30, unit: UnitSpeed.kilometersPerHour), schedule: Alert.Schedule.none)]) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.speedAlert.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isSendDefinitionsAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.SpeedAlertService.reloadViolations) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.speedAlert.isReloadViolationsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.speedAlert.reloadViolations { (result) in
                    switch  result{
                    case .success(let vio):
                        res = RespResult.success("\(vio)")
                    case .failure(let error):
                         res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isReloadViolationsAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.SpeedAlertService.deleteViolations) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.speedAlert.isDeleteViolationsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.speedAlert.deleteViolations { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.speedAlert.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isDeleteViolationsAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.GeofencingService.sendDefinitions) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.geofencing.isSendDefinitionsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                
                vehicle.geofencing.sendDefinitions([Geofencing.Definition.UserDefined(enabled: true, area: Alert.Area(zone: Alert.Area.Zone.green, areaType: Alert.Area.AreaType.rectangle(Alert.Rectangle(coordinate: Coordinate(latitude: latitude, longitude: longitude)!, width: 30, height: 30))), schedule: Alert.Schedule.none)]) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.geofencing.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isSendDefinitionsAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.GeofencingService.reloadViolations) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.geofencing.isReloadViolationsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.geofencing.reloadViolations { (result) in
                    switch  result{
                    case .success(let vio):
                        res = RespResult.success("\(vio)")
                    case .failure(let error):
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isReloadViolationsAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.GeofencingService.deleteViolations) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.geofencing.isDeleteViolationsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.geofencing.deleteViolations { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.geofencing.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isDeleteViolationsAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteBatteryChargeService.startCharging) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteBatteryCharge.isStartChargingAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteBatteryCharge.startCharging  { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remoteBatteryCharge.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isStartChargingAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteBatteryChargeService.stopCharging) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteBatteryCharge.isStopChargingAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteBatteryCharge.stopCharging { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remoteBatteryCharge.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isStopChargingAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteBatteryChargeService.requestCurrentChargerSettings) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteBatteryCharge.isRequestCurrentChargerSettingsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteBatteryCharge.requestCurrentChargerSettings { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remoteBatteryCharge.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isRequestCurrentChargerSettingsAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteBatteryChargeService.resetChargerSettings) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteBatteryCharge.isResetChargerSettingsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteBatteryCharge.resetChargerSettings { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remoteBatteryCharge.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isResetChargerSettingsAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteBatteryChargeService.selectChargeMode) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteBatteryCharge.isChargeModeSelectionAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteBatteryCharge.selectChargeMode(RemoteBatteryCharge.ChargerSettings.ChargeMode(value: RemoteBatteryCharge.ChargerSettings.ChargeMode.Value.immediateCharging, reason: nil, state: nil)) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remoteBatteryCharge.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isChargeModeSelectionAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteBatteryChargeService.allowAutomaticPlugUnlock) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteBatteryCharge.isSettingAutomaticPlugUnlockAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteBatteryCharge.allowAutomaticPlugUnlock(RemoteBatteryCharge.ChargerSettings.AutomaticPlugUnlockSettings(isACAllowedOnce: true, isACAllowedPermanent: true, isDCAllowedOnce: true, isDCAllowedPermanent: true)) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remoteBatteryCharge.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isSettingAutomaticPlugUnlockAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemotePretripClimatisationService.startClimatisation) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            let semahpore = DispatchSemaphore(value: 0)
            var res:RespResult?
            vehicle.remotePretripClimatisation.startClimatisation(with: RemotePretripClimatisation.ClimaterSettings(targetTemperature: nil, climatisationWithoutHVpower: nil, heaterSource: nil, elementSettings: nil), securePIN: nil) { (error) in
                if let error = error {
                    res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                } else {
                    res = RespResult.success("\(vehicle.remotePretripClimatisation.report.result!)")
                }
                semahpore.signal()
            }
            
            semahpore.wait()
            return res!
        }
        
        ADRequester.register(withKey: Services.RemotePretripClimatisationService.stopClimatisation) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remotePretripClimatisation.isStopClimatisationAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remotePretripClimatisation.stopClimatisation { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remotePretripClimatisation.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isStopClimatisationAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemotePretripClimatisationService.startWindowHeating) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remotePretripClimatisation.isStartWindowHeatingAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remotePretripClimatisation.startWindowHeating { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remotePretripClimatisation.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isStartWindowHeatingAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemotePretripClimatisationService.stopWindowHeating) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remotePretripClimatisation.isStopWindowHeatingAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remotePretripClimatisation.stopWindowHeating { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remotePretripClimatisation.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isStopWindowHeatingAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemotePretripClimatisationService.setClimaterSettings) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remotePretripClimatisation.isSetClimaterSettingsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remotePretripClimatisation.setClimaterSettings(settings: RemotePretripClimatisation.ClimaterSettings(targetTemperature: nil, climatisationWithoutHVpower: nil, heaterSource: nil, elementSettings: nil)) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remotePretripClimatisation.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isSetClimaterSettingsAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemotePretripClimatisationService.resetClimaterSettings) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remotePretripClimatisation.isResetClimaterSettingsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remotePretripClimatisation.resetClimaterSettings { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remotePretripClimatisation.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isResetClimaterSettingsAllowed == false", data: nil))
            }
        }
        
        
        ADRequester.register(withKey: Services.RemotePretripClimatisationService.requestCurrentClimaterSettings) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remotePretripClimatisation.isRequestCurrentClimaterSettingsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remotePretripClimatisation.requestCurrentClimaterSettings { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remotePretripClimatisation.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isRequestCurrentClimaterSettingsAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteDepartureTimeService.setProfiles) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            
            let semahpore = DispatchSemaphore(value: 0)
            var res:RespResult?
            vehicle.remoteDepartureTime.setProfiles([RemoteDepartureTime.Profile.Id.profile1 : RemoteDepartureTime.Profile(name: nil, operationCharging: true, operationClimatisation: true, targetChargeLevel: nil, nightRateActive: nil, nightRateStart: nil, nightRateEnd: nil, maximumChargeCurrent: nil, heaterSource: nil)], timers: [RemoteDepartureTime.Timer.Id.timer1 : RemoteDepartureTime.Timer(profileId: RemoteDepartureTime.Profile.Id.profile1, programmedStatus: RemoteDepartureTime.Timer.ProgrammedStatus.notProgrammed, frequency: RemoteTimer.Frequency.single(departureDate: Date(), departureTime: Time(hour: 1, minute: 1)!))], climaterSettings: nil, securePIN: nil) { (error) in
                if let error = error {
                    res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                } else {
                    res = RespResult.success("\(vehicle.remoteDepartureTime.report.result!)")
                }
                semahpore.signal()
            }
            
            semahpore.wait()
            return res!
            
        }
        
        ADRequester.register(withKey: Services.RemoteDepartureTimeService.requestCurrentSettings) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            let semahpore = DispatchSemaphore(value: 0)
            var res:RespResult?
            vehicle.remoteDepartureTime.requestCurrentSettings { (error) in
                if let error = error {
                    res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                } else {
                    res = RespResult.success("\(vehicle.remoteDepartureTime.report.result!)")
                }
                semahpore.signal()
            }
            
            semahpore.wait()
            return res!
            
        }
        
        ADRequester.register(withKey: Services.RemoteDepartureTimeService.resetSettings) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            let semahpore = DispatchSemaphore(value: 0)
            var res:RespResult?
            vehicle.remoteDepartureTime.resetSettings { (error) in
                if let error = error {
                    res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                } else {
                    res = RespResult.success("\(vehicle.remoteDepartureTime.report.result!)")
                }
                semahpore.signal()
            }
            
            semahpore.wait()
            return res!
            
        }
        
        ADRequester.register(withKey: Services.RemoteTripStatisticsService.reloadNewestTrip) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteTripStatistics.isLoadingTripsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteTripStatistics.reloadNewestTrip(of: RemoteTripStatistics.Trip.TripType.longTerm) { (result) in
                    switch result {
                    case .success(let trip):
                        res = RespResult.success("\(String(describing: trip))")
                    case .failure(let error):
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isLoadingTripsAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteTripStatisticsService.reloadTripsReport) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteTripStatistics.isLoadingTripsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteTripStatistics.reloadTripsReport(for: RemoteTripStatistics.Trip.TripType.shortTerm, in: DateInterval(start: Date(), duration: 1000)) { (result) in
                    switch result {
                    case .success(let trip):
                        res = RespResult.success("\(String(describing: trip))")
                    case .failure(let error):
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isLoadingTripsAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteTripStatisticsService.deleteAllTrips) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteTripStatistics.isDeleteAllTripsAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteTripStatistics.deleteAllTrips(of: RemoteTripStatistics.Trip.TripType.shortTerm) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("success")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isDeleteAllTripsAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.DestinationImportService.addDestination) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            
            let semahpore = DispatchSemaphore(value: 0)
            var res:RespResult?
            vehicle.destinationImport.addDestination(with: "测试地点", address: nil, coordinate: Coordinate(latitude: latitude, longitude: longitude)) { (result) in
                switch result {
                case .success(let dest):
                    res = RespResult.success("\(String(describing: dest))")
                case .failure(let error):
                    res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                }
                semahpore.signal()
            }
            
            semahpore.wait()
            return res!

        }
        
        ADRequester.register(withKey: Services.TheftAlarmService.reloadAlertHistory) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            let semahpore = DispatchSemaphore(value: 0)
            var res:RespResult?
            vehicle.theftAlarm.reloadAlertHistory { (result) in
                switch result {
                case .success(let obj):
                    res = RespResult.success("\(String(describing: obj))")
                case .failure(let error):
                    res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                }
                semahpore.signal()
            }
            
            semahpore.wait()
            return res!
            
        }
        
        ADRequester.register(withKey: Services.TheftAlarmService.loadHistoryActivated) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.theftAlarm.isLoadingHistoryActivatedAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.theftAlarm.loadHistoryActivated { (result) in
                    switch result {
                    case .success(let obj):
                        res = RespResult.success("\(String(describing: obj))")
                    case .failure(let error):
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isLoadingHistoryActivatedAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.TheftAlarmService.deleteAlertHistory) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.theftAlarm.isDeletingAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.theftAlarm.deleteAlertHistory { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.theftAlarm.report.result!)")
                    }
                    semahpore.signal()
                }
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isDeletingAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.TheftAlarmService.activateHistory) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.theftAlarm.isActivatingDeactivatingHistoryAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.theftAlarm.activateHistory { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.theftAlarm.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isActivatingDeactivatingHistoryAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.TheftAlarmService.deactivateHistory) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.theftAlarm.isActivatingDeactivatingHistoryAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.theftAlarm.deactivateHistory { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.theftAlarm.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isActivatingDeactivatingHistoryAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.VehicleTrackingService.loadSecureCenter) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.vehicleTracking.isLoadingSecureCenterAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.vehicleTracking.loadSecureCenter { (result) in
                    switch result {
                    case .success(let obj):
                        res = RespResult.success("\(String(describing: obj))")
                    case .failure(let error):
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isLoadingSecureCenterAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.VehicleTrackingService.loadSpecialModes) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.vehicleTracking.isLoadingSpecialModesAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.vehicleTracking.loadSpecialModes { (result) in
                    switch result {
                    case .success(let obj):
                        res = RespResult.success("\(String(describing: obj))")
                    case .failure(let error):
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isLoadingSpecialModesAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.VehicleTrackingService.downloadCertificatePDF) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            let semahpore = DispatchSemaphore(value: 0)
            var res:RespResult?
            vehicle.vehicleTracking.downloadCertificatePDF { (result) in
                switch result {
                case .success(let obj):
                    res = RespResult.success("\(String(describing: obj))")
                case .failure(let error):
                    res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                }
                semahpore.signal()
            }
            
            semahpore.wait()
            return res!
            
        }
        
        ADRequester.register(withKey: Services.VehicleTrackingService.getCertificateTypes) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.vehicleTracking.isLoadingCertificateTypesAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.vehicleTracking.getCertificateTypes{ (result) in
                    switch result {
                    case .success(let obj):
                        res = RespResult.success("\(String(describing: obj))")
                    case .failure(let error):
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isLoadingCertificateTypesAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.VehicleTrackingService.disable) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            
            let semahpore = DispatchSemaphore(value: 0)
            var res:RespResult?
            vehicle.vehicleTracking.disable(mode: VehicleTracking.SpecialMode.Mode.forcedDisarm, securePIN: nil) { (error) in
                if let error = error {
                    res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                } else {
                    res = RespResult.success("\(vehicle.vehicleTracking.report.result!)")
                }
                semahpore.signal()
            }
            
            semahpore.wait()
            return res!
            
        }
        
        ADRequester.register(withKey: Services.VehicleTrackingService.set) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            let semahpore = DispatchSemaphore(value: 0)
            var res:RespResult?
            vehicle.vehicleTracking.set(duration: Measurement(value: 1, unit: UnitDuration.minutes), for: VehicleTracking.SpecialMode.Mode.forcedDisarm, securePIN: nil) { (error) in
                if let error = error {
                    res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                } else {
                    res = RespResult.success("\(vehicle.vehicleTracking.report.result!)")
                }
                semahpore.signal()
            }
            
            semahpore.wait()
            return res!
            
        }
        
        
        ADRequester.register(withKey: Services.VehicleTrackingService.loadSetupStatus) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.vehicleTracking.isLoadingSetupStatusAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.vehicleTracking.loadSetupStatus { (result) in
                    switch result {
                    case .success(let obj):
                        res = RespResult.success("\(String(describing: obj))")
                    case .failure(let error):
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isLoadingSetupStatusAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.AssistanceCallService.loadAssistancePhoneNumber) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.assistanceCall.isLoadingAssistancePhoneNumberAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                do {
                    try vehicle.assistanceCall.loadAssistancePhoneNumber(for: AssistanceCall.Reason.incident, carrier: AssistanceCall.CarrierIdentifier(countryCode: "123", networkCode: "net")) { (result) in
                        switch result {
                        case .success(let obj):
                            res = RespResult.success("\(String(describing: obj))")
                        case .failure(let error):
                            res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                        }
                        semahpore.signal()
                        
                    }
                    semahpore.wait()
                    return res!
                } catch {
                    res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    return res!
                }
            } else {
                return RespResult.failure(RespError(code: "", msg: "isLoadingAssistancePhoneNumberAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.ServiceAppointmentService.set) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.serviceAppointment.isSettingConfigurationAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.serviceAppointment.set(configuration: ServiceAppointment.Configuration(customer: nil, servicePartner: nil)) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.serviceAppointment.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isSettingConfigurationAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.ServiceAppointmentService.loadConfiguration) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.serviceAppointment.isLoadingConfigurationAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.serviceAppointment.loadConfiguration { (result) in
                    switch result {
                        case .success(let obj):
                            res = RespResult.success("\(String(describing: obj))")
                        case .failure(let error):
                            res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                        }
                        semahpore.signal()
                    }
                    
                    semahpore.wait()
                    return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "<##> == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.ServiceAppointmentService.activate) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.serviceAppointment.isActivationAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.serviceAppointment.activate { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.serviceAppointment.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isActivationAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.ServiceAppointmentService.deactivate) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.serviceAppointment.isDeactivationAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.serviceAppointment.deactivate { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.serviceAppointment.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isDeactivationAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteProfileTimerService.setTimers) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteProfileTimer.isSetTimersAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteProfileTimer.setTimers([RemoteProfileTimer.Timer(id: RemoteProfileTimer.Timer.Id.timer0, activationState: RemoteProfileTimer.Timer.ActivationState.activated, frequency: RemoteTimer.Frequency.single(departureDate: Date(), departureTime: Time(hour: 1, minute: 1)!), chargerSettings: RemoteProfileTimer.Timer.ChargerSettings(chargeOption: RemoteProfileTimer.Timer.Option.enabled, targetStateOfCharge: nil), climatisationOption: RemoteProfileTimer.Timer.Option.enabled, climatisationSettings: nil)]) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remoteProfileTimer.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isSetTimersAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteProfileTimerService.setProfiles) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteProfileTimer.isSetProfilesAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteProfileTimer.setProfiles([RemoteProfileTimer.Profile(id: RemoteProfileTimer.Profile.Id(rawValue: 1), name: nil, isDeactivated: true, timers: nil, profileOptions: nil, chargingOptions: nil, powerLimitation: nil, position: nil)]) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remoteProfileTimer.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isSetProfilesAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.RemoteProfileTimerService.SetTimersAndProfiles) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.remoteProfileTimer.isSetTimersAndProfilesAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.remoteProfileTimer.setTimers([RemoteProfileTimer.Timer(id: RemoteProfileTimer.Timer.Id.timer0, activationState: RemoteProfileTimer.Timer.ActivationState.activated, frequency: RemoteTimer.Frequency.single(departureDate: Date(), departureTime: Time(hour: 1, minute: 1)!), chargerSettings: RemoteProfileTimer.Timer.ChargerSettings(chargeOption: RemoteProfileTimer.Timer.Option.enabled, targetStateOfCharge: nil), climatisationOption: RemoteProfileTimer.Timer.Option.enabled, climatisationSettings: nil)], and: [RemoteProfileTimer.Profile(id: RemoteProfileTimer.Profile.Id(rawValue: 1), name: nil, isDeactivated: true, timers: nil, profileOptions: nil, chargingOptions: nil, powerLimitation: nil, position: nil)]) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.remoteProfileTimer.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isSetTimersAndProfilesAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.ETronRoutePlanningService.loadConsumptionData) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.eTronRoutePlanning.isLoadingConsumptionDataAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.eTronRoutePlanning.loadConsumptionData(latitude: latitude, longitude: longitude, departureTime: Date()) {
                    (result) in
                    switch result {
                    case .success(let obj):
                        res = RespResult.success("\(String(describing: obj))")
                    case .failure(let error):
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
                
                
            } else {
                return RespResult.failure(RespError(code: "", msg: "isLoadingConsumptionDataAllowed == false", data: nil))
            }
        }
        
        ADRequester.register(withKey: Services.PlugAndChargeService.activate) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.plugAndCharge.isActivateAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.plugAndCharge.activate(securePIN: nil) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.plugAndCharge.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isActivateAllowed == false", data: nil))
            }
        }
    
        ADRequester.register(withKey: Services.PlugAndChargeService.deactivate) { (userInfo) -> RespResult in
            guard let vehicle = ADMediator.shared.vehicle else {
                return Result.failure(RespError(code: "", msg: "unlogin", data: nil))
            }
            
            if vehicle.plugAndCharge.isDeactivateAllowed {
                let semahpore = DispatchSemaphore(value: 0)
                var res:RespResult?
                vehicle.plugAndCharge.deactivate(securePIN: nil) { (error) in
                    if let error = error {
                        res = RespResult.failure(RespError(code: "", msg: "\(error)", data: nil))
                    } else {
                        res = RespResult.success("\(vehicle.plugAndCharge.report.result!)")
                    }
                    semahpore.signal()
                }
                
                semahpore.wait()
                return res!
            } else {
                return RespResult.failure(RespError(code: "", msg: "isDeactivateAllowed == false", data: nil))
            }
        }
    }
}

