//
//  ADRequester.swift
//  GSDKDemo
//
//  Created by KingYoung on 2020/05/13.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import UIKit

typealias RespResult = Result<Any, RespError>

struct RespError: RespErrorAnalysable, Error {
   
    var code: String
    
    var msg: String
    
    var data: [AnyHashable : Any]?
}

protocol RespErrorAnalysable {
    //错误码 如：“5000038”、“142307”、“-1001”等
    var code: String { get }
    //错误信息 如：“登录已过期”、“机构代码与发票不匹配”等
    var msg: String { get }
    //错误数据 接口返回原始数据 如：
    /*
     {
      code: "12345"
      data: {
        "date": "2009/20/20"
      }
      message: "登录已过期"
     }
     */
    var data: [AnyHashable : Any]? { get }
}





typealias ADRequestHandler = (_ routerParameters: [String: Any]?) -> RespResult

struct ADRequester {
   
    private static var handlers: [AnyHashable : ADRequestHandler] = [:]
    
    static func request(withKey key: AnyHashable, userInfo: [String: Any]?, completion: ((_ result: RespResult)->())?) {
        if let handler = handlers[key] {
            
            DispatchQueue.global().async {
                completion?(handler(userInfo))
            }
        }
    }
    
    static func register(withKey key: AnyHashable, handler: @escaping ADRequestHandler) {
        handlers[key] = handler
    }

}

